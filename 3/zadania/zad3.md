## Zadania domowe
### Arytmetyka
Napisz skrypt `mat.sh`, który wczyta od uzytkownika 2 argumenty, a następnie poda ich sumę, różnicę, iloczyn i iloraz

### Liczenie plików
Napisz skrypt, który policzy pliki w bieżącym folderze (zakładamy, że w folderze nie ma katalogów).

### Statystyki w plikach
W  folderze `pliki` mamy pliki zawierające różne pliki tekstowe. Napisz skrypt, który dla każdego z pliku poda:

* liczbę słów 

* liczbę linii

* średnia gęstość znaków w słowie (całkowita liczba znaków podzielona przez całkowitą liczbę słów)

* średnia gęstość słów na linię (całkowita liczba słów podzielona przez całkowitą liczbę linii)

* ile linijek w tekście zawiera słowo "nigga"

* ile linijek w tekście zawiera słowo "bitch"

### Łączenie plików
Napisz skrypt, który:

1. Stworzy folder `~/lyrics`

2. Przekopiuje tam zawartość katalogu `zadania/pliki` z tych zajęć

3. Z pierwszych 5 linijek każdego z plików utworzy jeden zbiorczy "tekst" (kolejność dowolna)

4. Wykasuje wszystkie pliki poza utworzonym tekstem

### Zadanie z gwiazdką - I love my bitch
Spróbuj i napisz skrypt, który zamieni wszystkie słowa "momma" (dowolnej wielkości liter) na "bitch" w pliku `zadania/pliki/i_love_my_momma.txt`
UWAGA: Potrzebny będzie StackOverflow i programik `sed`. (o tym na kolejnych zajęciach).
