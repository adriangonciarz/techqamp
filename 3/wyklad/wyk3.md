### Budowa systemu Linux
[Diagram budowy systemu Linux](https://www.tutorialspoint.com/operating_system/images/linux_architecture.jpg)
#### Główne komponenty
* Jądro - Jądro i jego moduły odpowiadają za komunikację z urządzeniami. Bardzo niskopoziomowe operacje, raczej nie modyfikowane przez średnio-zaawansowanych użytkowników. Jądro wszystkich dystrybucji systemu Linux jest wspólne. i rozwijane w ramach The Linux Foundation.
* Biblioteki systemowe - funkcje i programy, które komunikują się z zasobami jądra systemu "w imieniu" innych aplikacji wysokiego poziomu
* Narzędzia systemowe - realizują wyspecjalizowane zadania w obrębie systemu (np. z procesami, pamięcią)

####Dystrybucje
Mają wspólne jądro ale różne aplikacje, powłoki, etc.
[Diagram dystrybucji Linuxa](https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg)

### Shell - powłoka systemu operacyjnego
Shell to najprościej mówiąc program, który interpretuje komendy wprowadzane przez użytkownika. Inaczej mówiąc: shell jest interpreterem pewnego języka skryptowego (systemowego). Rodzajów powłok jest bardzo wiele, w zasadzie każdy może napisać swój shell. Najpopularniejszą powłoką używaną domyślnie przez Ubuntu jest shell BASH, czyli tzw. Bourne Again shell.
Wszystkie polecenia systemowe, takie jak `cat`, `grep`, etc. to element powłoki. Powłoka posiada też zmienne, iteratory, warunki logiczne, etc. Powłoka jest zatem pełnoprawnym, choć niezbyt wygodnym na dużą skalę, językiem programowania. Najczęściej wykorzystuje się go tworząc skrypty powłokowe, które pomagają nam w powtarzalnych, skomplikowanych czynnościach w w systemie.

#### Pierwszy skrypt - wydruk
Komentarz - linijka rozpoczynająca się od znaku `#`, nie jest wykonywana.
Stwórz plik `skrypt1.sh` z zawartością
```
#!/bin/sh
echo "Hello, World!"
```
Pierwsza linijka jest szczególna, mówi o rodzaju użytej powłoki (SH), druga drukuje linijkę "Hello, World!". zmień prawa dla pliku przez `chmod 700 skrypt1.sh` i uruchom poleceniem `./skrypt1.sh`

####Uruchamianie skryptów
`./skrypt.sh`
`bash skrypt.sh`
`sh skrypt.sh`
Oczywiście nazwa skryptu nie musi zawierać rozszerzenia `.sh`, ale plik musi być wykonywalny

####Zmienne środowiskowe
System przetrzymuje pewne ważne informacje w tzw. zmiennych środowiskowych. 
Można je podejrzeć poleceniem `env`
Ważne zmienne: `$PATH` `$USER`, `$HOME`
Można ustawić własną zmienną środowiskową poleceniem `export`, np. `export $DUPA=dupa`. Potem możemy podejrzeć tę zmienną poleceniem `echo`: `echo $DUPA`. Znak dolara służy do odnoszenia się do zmiennej po zadeklarowaniu.

#### Zmienne w skryptach
Przy uruchomieniu skryptu można podać argument(y) bezpośrednio po nazwie skryptu, tj. `./skrypt.sh zmienna`. Skrypty przetrzymują kolejne argumenty jako zmienne $1, $1, $3, etc.
Zmień `skrypt1.sh`
```
#!/bin/sh
VAR1="$1"
echo "Hello, $VAR1!"
```
Uruchom:
`./skrypt1.sh Adrian`

Stwórz `skrypt2.sh`
```
#!/bin/sh
VAR1="$1"
STATIC="Milo Cie poznac, "
echo $STATIC $VAR1
```
Zmień prawa na 700 i Uruchom `sh skrypt2.sh Adrian`

Użyj zmiennych środowiskowych, najpierw zadeklaruj nową:
`export FIRMA=DREDAR`
potem skorzystaj z niej w skrypcie
```
#!/bin/sh
echo "Witaj, $USER"
echo "Czy to prawda, ze pracujesz w $FIRMA ?"
```

Argumentem dla naszego skryptu może być oczywiście plik (i do tego używa się skryptów bardzo często). Przyklad:
```
#!/bin/sh
old_file=$1
new_file=$2
head -n 5 $old_file > $new_file

```
potem
`./skrypt.sh pliki/lyrics.txt excerpt.txt`

####Uruchamiane komend wewnątrz ECHO
Żeby uruchomić komendę w stringu należy użyć `backticks`, czyli specjalnego "cudzysłowia", np.
```
echo "Jestes teraz w folderze `pwd`"
```
```
#!/bin/sh
echo "Dzisiejsza data: `date`"
```

#### Wczytywanie zmiennych
Do pobierania od użytkownika treści służy funkcja `read`
```
#!/bin/sh
clear
echo "Podaj swoje imie"
read imie
echo "Czesc, $imie"
```

#### Arytmetyka
Skrypty mogą zawierać operacje matematyczne. operacje można przypisać do zmiennej przez `$((wyrazenie))` lub `$[wyrazenie]`
```
suma=$((8+2))
roznica=$((7-2))
iloraz=$[7/2]
iloczyn=$[66*11]
modulo=$((5%2))
```
Żeby to wydrukować:
```
echo "$((7*2))"
echo "$roznica"
```
Na przyklad
```
#!/bin/bash
roznica=$[6-2]
echo "$roznica"
```
Skrypt, który doda 10 do wprowadzonego argumentu
```
#!/bin/sh
a=$1
echo "$[$a+10]"
```
Do przeprowadzenia obliczeń można też skorzystać z polecenia let. Przykład:
```
#!/bin/bash
liczba1=5
liczba2=6
let wynik=liczba1*liczba2
echo $wynik
```

#### Dowolna liczba plików i iteracje
Gdybyśmy chcieli do skryptu podać dowolną ilość plików (co najczęściej się zdarzy), możemy to zrobić za pomocą tablicy `$@`. W samym skrypcie możemy iterować po tej tablicy
Przykladowy skrypt, ktory szuka w dodowlnej ilosci podanych plików wyrażenia 'wish':
```
#!/bin/bash
for file in "$@"
do
  echo $file
done
```
Skrypt, który przyjmie dowolną liczbę plików i wyszukwa w każdym jakiegoś wyrażenia. Zastosujmy ten skrypt do wyrażenia "wish" w plikach `lyrics` w folderze `pliki`
```
#!/bin/bash
phrase="wish"
for file in "$@"
do
  echo "------------Plik: $file--------------"
  echo "Znalezione linie z wyrazeniem 'wish':"
  echo "-------------------------------------"
  grep $phrase $file
done
```