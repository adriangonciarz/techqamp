##Przetwarzanie strumienia
Polecenie `cat` tworzy strumień z klawiatury
Przerwanie strumienia `cat` - `ctrl+D`

Przekierowanie strumienia  na zewnątrz `>`
Przekierowanie strumienia do wewnątrz `<`
Np.
```
% echo "Mary had a little lamb" > mary.txt
% cat mary.txt
% cat > mcdonald.txt
Old McDonald had a farm
% cat < mcdonald.txt 
```
Można przekierowywać strumień z pliku do pliku
`cat < mcdonald.txt > copy_of_mcdonald.txt`
Operator `>>` powoduje dopisanie strumienia do pliku (append)
Wcześniej widzieliśmy to już w przypadku `echo "Jakis tekst" >> plik.txt`
Najprostszy edytor tekstowy świata
`cat >> tekst_z_klawiatury.txt`
Można też łączyć pliki:
`cat plik1 plik2 > plik_wyjsciowy`

##Przetwarzanie potokowe
`head` - wyświetla początek pliku
`tail` - wyświetla koniec pliku
* `-c` (ile znaków)
* `-n` (ile linii)
`tailf` - użyteczny alias

`sort` - sortuje linie w pliku
* `-n` (numerycznie)
* `-k X` (według X-tej kolumny)
* `-r` (odwrotnie)

`uniq` - pokazuje wiersze bez powtórzeń
* `-d` (tylko powtarzające się)
* `-u` (tylko unikatowe)

`wc` - zlicza
* `-l` (linie)
* `-w` (słowa)
* `-c` (znaki)

`grep` - przeszukuje strumień w poszukiwaniu ciągu znaków
* `-v` (linie nie zawierające szukanego wzorca)
* `-c` (podaje liczbę odszukanych wyrażeń)
* `-l` (nazwy plików zawierających określony wzorzec)