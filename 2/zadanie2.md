1. Za pomocą polecenia `cat >` utwórz plik `sort1` o zawartości
```
1
12
3
56
23
8
2
134
22
```
* Wyświetl zawartości pliku
* Wyświetl zawartość pliku posortowaną alfabetycznie (pierwsze znaki)
* Wyświetl zawartość pliku posortowaną numerycznie (wartości liczbowe)
2. Utwórz plik `sort2` o zawartości
```
1 Adi
65 Kutys
3 Slawek
5 Rytmus
12 Sadowski
2 Grazyna
12 Sumatra
3 Abdul
```
* posortuj plik według pierwszej kolumny
* posortuj plik według drugiej kolumny i zapisz wynik do pliku `sorted_names`
3. Dopisz do pliku `sort1` wiersze
```
22
12
1
1
8
```
* wyswietl plik posortowany numerycznie
* Wyświetl tylko unikalne wiersze
* policz ile jest unikatowych liczb w pliku

4. Przeszukaj plik `logs/error.log` pod kątem frazy `robots.txt`
5. Wyświetl wszystkie błędy (linie) z pliku `logs/errorl.log` inne niż `Connection reset by peer`
6. Używając pliku `access.log` sprawdź ile razy odwiedzono adres `/twiki/bin/rdiff/TWiki/SvenDowideit`
7. Z pliku `access.log` wyświetl linie, które nie zawierają requestu `GET`