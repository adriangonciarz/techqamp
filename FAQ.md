####Jak odpalić plik tekstowy jezeli nie chce użyć nano?
Podejrzeć jego zawartość możesz poleceniem cat, tail, head, etc. 
Jeśli chodzi o edytory to np. vim

####Jak odpalac w ogole pliki bez konkretnych programow?
w sensie używania terminala pliki można wykonywać jako skrypty shellowe, można je przetwarzać potokowo, albo mogą to być pliki wsadowe innych programów. Przykładem są kompilatory: kompilator gcc albo javac bierze pliki z kodem i tworzy z tego obraz binarny albo np. JAR. W tym sensie "uruchamia się pliki i programy"

####Jak działa whatis i apropos?
Obie te komendy wybierają z bazy danych informację o komendach systemowych, np whatis grep przeszuka system pod kątem informacji o programie grep. Analogicznie działa apropos. Niezbyt użyteczne na dłuższą metę.

####Dlaczego help i man są zrobione oddzielnie?
help jest chyba stricte systemową pomocą, a manual (man) każdy twórca programu sobie może dodać 

####Czym się różni kopiowanie plików orazdowiązanie twarde i symboliczne? 
Kopiowanie plików robi je całkowicie niezależnymi, niezwiązanymi ze sobą. Dwa osobne pamięci, zero wzajemnego wpływu.
Link twardy A do B: B jest fizyczną kopią obszaru pamięci A i są od siebie obustronnie zależne. W twardej relacji A-B zmiana A wpłynie na B i odwrotnie. Zniszczenie A zniszczy B.
Link symboliczny z A do B to podpięcie adresu obszaru pamięci pliku A do B. Zmiany dziąłają podobnie jak w przypadku twardej relacji (A zmienia B i odwrotnie), ale zniszczenie A niszczy B, natomiast zniszczenie B pozostawia A bez zmian.

####Czy da się odlinkować twarde dowiązanie?
Da się, poleceniem unlink. To "niszczy" ten drugi, podlinkowany plik, zostawia natomiast oryginał. Innym sposobem na zniszczenie dowiązania twardego ejst usunięcie któregokolwiek ze zlinkowanych plików.