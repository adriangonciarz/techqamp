###Poruszanie się

* `whoami` - pokazuje na jakim userze jestem
* `pwd` - sciezka do aktualnego katalogu
* `cd /` - przejscie do nadrzednego katalogu na danym dysku
* `cd ~` - przenosi do mojego katalogu domowego (usera zalogowanego)
* `cd ..` - przejście do folderu wyżej
* `clear` - czysci zawartość konsoli
* `help` - wywala komendy
* `man pwd` - pokaże pomoc dla pwd

###Chodzenie po pomocy man:
* `spacja` - przejście do następnej strony 
* `Ctrl+B` - przejście do poprzedniej strony 
* `q` - zamknięcie i opuszczenie przeglądarki;
* `/` - wyszukiwanie tekstu w przód, po znaku / należy wpisać tekst do wyszukania;
* `?` - wyszukiwanie w tył;
* `n, N` - przejście do następnego /poprzedniego  wystąpienia poszukiwanego wyrażenia. 

###Listowanie zawartości
* `ls` - wylistowanie zawartosc folderu
* `ls -a` - /\ + ukryte
* `ls -l` - /\ + kolumny
* `ll` - alias dla polecenia ls -la
* `ll /home/adam/Videos` - pokazuje konkretny folder (musi być pełna ścieżka)
 
### Kolumny w poleceniu ls -l:
np. 
`-rw-r--r-- 1 adriangonciarz  staff 34586  3 cze 2015 CV.odt`
* 1 - typ i uprawnienia (patrz niżej)
* 2 - ilość linków do tego obiektu 
* 3 - mowi kto stworozyl plik
* 4 - jaka grupa jest ownerem/moze edytowac 
* 5 - to wielkosc w bajtach 
* 6 - data utworzenia
* 7 - nazwa

###Katalogi
`mkdir nazwa` - nowy katalog o nazwie nazwa
`mkdir nazwa1/nazwa2` - stworzy katalog nazwa2 w folderze nazwa1
`mkdir ../nazwa`- stworzy katalog nazwa w katalogu-matce
`rm -rf` - skasuje katalog

###Pliki
`touch nazwa` - tworzy pusty plik o nazwie nazwa np. `touch adam.txt`
`rm nazwa` - usuwa plik o nazwie nazwa
`nano` - edytor textowy
`nano plik` - otworzy plik w programie nano
`ctrl+x` - wyjście z nano (zapyta o zapisanie pliku i nazwe)

`cat nazwa.txt` - "czyta" plik
`TAB` - dopelnia nazwe 
`echo "Cos Nowego" >> adam.txt` - tworzy plik z zawartością "Coś nowego" 
`cp plikA plikB` - kopiuje plikA jako plikB


Wyszukiwanie:
`locate kawa` - wyszukuje w strukturze plik `kawa`
`Locate kaw*` - wyszukuje wszystkie pliki zaczynające się od `kaw`
`Find adam` - wyszuka wszystkie pozycje z adam

###Uprawnienia 

Po poleceniu ll plik w pierwszej kolumnie ma wylistowane uprawnienia: drwxrwxrwx
pierwszy znak oznacza typ: - plik, d folder, l link
rwx - pierwsza odnośnie ownera, druga to grupa ownera a trzecia to wszyscy inni

###Zmiana uprawnień:
chmod UGO - zmiana uprawnien 
Gdzie UGO odpowiada zakresowi uprawnień: U - owner, G - grupa ownera, O - inni
4 - read
2 - write
1 - execute
0 - nic (brak uprawnień)
`chmod 123` (kazda cyfra innny poziom uprawnien + jest suma uprawnien z gory)
`chmod 740 dupa`
`./dupa` - odpalenie spryptu wykonywalnego 

Dowiązania:
`ln ~/Lekcje/Adam/kawa.txt kawa1.txt` - tworzy dowiązanie twarde (kawa1) do pliku kawa z katalogu Adam
`ln -2 /Lekcje/Adam/kawa.txt /plik1.txt` - tworzy dowiazanie symboliczne (plik1.txt) do pliku z katalogu Adam w katalogu domowym