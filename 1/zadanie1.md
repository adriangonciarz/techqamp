###Operacje na plikach i folderach
1. Przejdź do katalogu nadrzędnego "/"
2. Przejdź do katalogu `/var` i stwórz w nim katalog `www` (jeśli nie istnieje). Wejdź do katalogu
3. Utwórz plik bez zawartości `config.yml`
4. Skopiuj ten plik z nową nazwą `config_old.yml`. Ile teraz jest plików w folderze?
5. Usuń plik config `config.yml`
6. Za pomocą programu `nano` zedytuj plik `config_old.yml` i wstaw w nim zawartość
```
baseurl:
  host.mojserwer.com
default_port:
  8080
default:
  supported_languages:
    de: Deutsch
    fr: Francais
    en: English
info:
  version: 2.0.0
  datum: 11.05.2015
``` 
Zapisz plik jako `config.yml`. Ile teraz jest plików w folderze?
7. Usuń plik `config_old.yml`
8. Wylistuj pliki w folderze. Ile ich jest?
9. Z użyciem polecenia terminalowego `echo` stwórz plik `ave.txt` o treści (w miejsce kropek wstaw swoje imię)
```
Chwała Ci wspaniały ...!!!
```
10. utwórz link do pliku `ave.txt` w swoim folderze domowym (`~`)
11. Przejdź do katalogu domowego używając `~`. W jakim folderze teraz jesteś?
12. Wylistuj pliki w tym folderze i pokaż, że utworzono poprawnie link
13. Wyświetl zawartość tego pliku
14. Z poziomu tego folderu (domowego) zedytuj plik `ave.txt` w następujący sposób:
```
echo 'Chwała Ci wspaniały ...!!!'
```
15. Nie zmieniając folderu wyświetl zawartość pliku `/var/www/ave.txt`. Czy zawartość się zmieniła? Czemu?
16. W dowolny sposób spraw, aby plik miał uprawnienia `rwx-r--r--`
17. Uruchom ten *skrypt*